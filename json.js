const tabList = [{
	name: '最新',
	id: '1',
	cateid:'0'
}, {
	name: 'NetCore',
	id: '2',
	cateid:'101'
}, {
	name: 'Java',
	id: '3',
	cateid:'93'
}, {
	name: 'Python',
	id: '4',
	cateid:'140'
}, {
	name: '人工智能',
	id: '5',
	cateid:'57'
}, {
	name: '前端',
	id: '6',
	cateid:'52'
}, {
	name: '移动端',
	id: '7',
	cateid:'54'
}, {
	name: '运维',
	id: '8',
	cateid:'63'
}, {
	name: '资讯',
	id: '9',
	cateid:'292'
}];


export default {
	tabList
}
